import random
import discord
import asyncio
import requests
from var import *

commands=['!droll','!roll20','!spellsearch','!shadowroll']
client=discord.Client()
'''
def monsearch(mon):
    mon=mon.lstrip()
    monL=[]
    if len(mon.split()) > 1:
        mon=mon.replace(' ','+')
    else:
        pass   
    URL='http://dnd5eapi.co/api/monsters/?name='+mon.title()

    with requests.get(url=URL) as r:
        data=r.json()
        newURL=data['results'][0]['url']
    with requests.get(url=newURL) as r:
        newData=r.json()
    monL.append(newData['na'])
    return str(newData['desc']).replace("['","").replace("']","")
'''
def shadowroll(value):
    value=int(value.lstrip())
    shadowmiss=0
    shadowrollEval=0
    for i in range(1,value):
        #shadowrollEval+=random.randint(1,6)
        roll=random.randint(1,6)
        if roll == 1 or roll == 2:
            shadowmiss+=1
        elif roll == 5 or roll == 6:
            shadowrollEval+=1
        else:
            pass

    return [(str(shadowrollEval)+' hits'),(str(shadowmiss)+' misses')]

def spellSearch(spell):
    spell=spell.lstrip()
    if len(spell.split()) > 1:
        spell=spell.replace(' ','+') 
    else:
        pass
    URL='http://dnd5eapi.co/api/spells/?name='+spell.title()
    with requests.get(url=URL) as r:
        data=r.json()
        newURL=data['results'][0]['url']
    with requests.get(url=newURL) as r:
        newData=r.json()

    return str(newData['desc']).replace("['","").replace("']","")
    '''
    r=requests.get(url=URL)
    data=r.json()
    newURL=data['results'][0]['url']
    r=requests.get(url=newURL)
    newData=r.json()
    #spellList.append(newData['name'])
    #spellList.append(newData['desc'])
    outPut=str(newData['desc'])
    return outPut.replace("['","").replace("']","")
    #return str(newData['desc']).replace("['","").replace("']","")
    '''
def droll(value):
    a=int(value.split('d')[0])
    b=int(value.split('d')[1])
    rolleval=0
    for i in range(0,a):
         rolleval+=random.randint(1,b)
    return rolleval
    
def roll20():

    return random.randint(1,20)

'''
def main():

    
    typeroll=input('roll20 or droll: ')
    if typeroll.lower() == 'roll20':
        roll20()
    elif typeroll.lower() == 'droll':
        value=input('data: ' )
        int(value.split('d')[0]),int(value.split('d')[1])
    else:
        print('no value')
main()
'''
@client.event
async def on_message(message):

    #damage or other roll
    if message.content.startswith(commands[0]):
        if message.author.id != client.user.id:
            value=message.content.split(commands[0])[1]
            await client.send_message(message.channel,'{0} {1}'.format(message.author.mention,droll(value)))

    #single d20 roll
    elif message.content.startswith(commands[1]):
        if message.author.id != client.user.id:
            value=message.content.split(commands[1])[1]
            await client.send_message(message.channel,'{0} {1}'.format(message.author.mention,roll20()))

    #spell search

    elif message.content.startswith(commands[2]):
        if message.author.id != client.user.id:
            spell=message.content.split(commands[2])[1]
            await client.send_message(message.channel,'{0} {1}'.format(message.author.mention,spellSearch(spell)))

    #shadowrun roll
    elif message.content.startswith(commands[3]):
        if message.author.id != client.user.id:
            value=message.content.split(commands[3])[1]
            await client.send_message(message.channel,'{0} {1}'.format(message.author.mention,str(shadowroll(value)).replace("['","").replace("']","").replace("', '"," ")))

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
client.run(tokenbot)